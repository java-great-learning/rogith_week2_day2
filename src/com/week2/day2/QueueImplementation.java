package com.week2.day2;

import java.util.Iterator;
import java.util.PriorityQueue;

public class QueueImplementation {

	public static void main(String[] args) {
		 PriorityQueue<Integer> pQueue = new PriorityQueue<Integer>();
		 
	        
	        pQueue.add(10);
	        pQueue.add(20);
	        pQueue.add(15);
	 
	        
	        System.out.println(pQueue.peek());
	        
	        System.out.println(pQueue.size());
	       
	        System.out.println(pQueue.element());
	      
	        System.out.println(pQueue.poll());
	       
	        System.out.println("\n\nThe PriorityQueue elements:"); 
	        Iterator iter1 = pQueue.iterator(); 
	        while (iter1.hasNext()) 
	            System.out.println(iter1.next() + " "); 
	        pQueue.clear();
	        System.out.println(pQueue.isEmpty());

	}

}
