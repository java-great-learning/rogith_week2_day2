package com.week2.day2;

public class Brand {
	public int brandid;
	public String brandname;
	public String location;
	public Brand(int brandid, String brandname, String location) {
		super();
		this.brandid = brandid;
		this.brandname = brandname;
		this.location = location;
	}
	public Brand() {
		
	}
	public int getBrandid() {
		return brandid;
	}
	public void setBrandid(int brandid) {
		this.brandid = brandid;
	}
	public String getBrandname() {
		return brandname;
	}
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Override
	public String toString() {
		return "Brand [brandid=" + brandid + ", brandname=" + brandname + ", location=" + location + "]";
	}

}
