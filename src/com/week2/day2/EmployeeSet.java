package com.week2.day2;

import java.util.HashSet;
import java.util.Set;



public class EmployeeSet {

	public static void main(String[] args) {
		Set<Employee> employees=new HashSet<Employee>();
		employees.add(new Employee(1, "a", "Chennai"));
		employees.add(new Employee(2, "b", "Chennai"));
		employees.add(new Employee(1, "a", "Chennai"));
		
		
		System.out.println(employees.size());
		for(Employee emp:employees)
			System.out.println(emp);

	}

}