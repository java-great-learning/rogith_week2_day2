package com.week2.day2;

public class Product {
	public int proid;
	public String proname;
	public String description;
	public Brand brand;
	public int price;
	public Product(int proid, String proname, String description, Brand brand, int price) {
		super();
		this.proid = proid;
		this.proname = proname;
		this.description = description;
		this.brand = brand;
		this.price = price;
	}
	public Product() {
		
	}
	public int getProid() {
		return proid;
	}
	public void setProid(int proid) {
		this.proid = proid;
	}
	public String getProname() {
		return proname;
	}
	public void setProname(String proname) {
		this.proname = proname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [proid=" + proid + ", proname=" + proname + ", description=" + description + ", brand=" + brand
				+ ", price=" + price + "]";
	}
	


}