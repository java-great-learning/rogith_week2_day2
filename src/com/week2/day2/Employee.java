package com.week2.day2;

import java.util.Objects;

public class Employee implements Comparable<Employee> {
	private int eid;
	private String ename;
	private String city;
	public Employee(int eid, String ename, String city) {
		super();
		this.eid = eid;
		this.ename = ename;
		this.city = city;
	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Employee [eid=" + eid + ", ename=" + ename + ", city=" + city + "]";
	}



	@Override
	public int hashCode() {
		return Objects.hash(city, eid, ename);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return Objects.equals(city, other.city) && eid == other.eid && Objects.equals(ename, other.ename);
	}

	@Override
	public int compareTo(Employee o) {
		// TODO Auto-generated method stub
		return this.getEname().compareTo(o.getEname());
	}

}