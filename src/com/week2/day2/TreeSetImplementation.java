package com.week2.day2;

import java.util.TreeSet;

public class TreeSetImplementation {

	public static void main(String[] args) {
		TreeSet<String> t1 =new TreeSet<>();
		t1.add("orange");
		t1.add("apple");
		t1.add("banana");
		t1.add("mango");
		t1.add("kiwi");
		System.out.println(t1);
		
		for(String string : t1) {
			System.out.println(string);
		}
		System.out.println(t1.first());
		System.out.println(t1.last());
		System.out.println(t1.pollLast());
		System.out.println(t1);
		System.out.println(t1.size());
		

	}

}
