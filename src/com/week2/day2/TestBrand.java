package com.week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
public class TestBrand {

	public static void main(String[] args) {
		Brand brand1= new Brand(1,"golddrop","India");
		Brand brand2= new Brand(1,"fortune","India");
		Brand brand3= new Brand(1,"golddrop","India");
		Brand brand4= new Brand(1,"fredom","India");
		Brand brand5= new Brand(1,"freedrop","India");
		
		List<Product> products=new ArrayList<Product>();
		products.add(new Product(1,"one liter","ABC",brand1, 600));
		products.add(new Product(2,"half liter","CDA",brand2,200));
		products.add(new Product(3,"5 liter","ZXT",brand3,750));
		products.add(new Product(4,"3 liter","KLM",brand4,780));
		products.add(new Product(5,"2 liter","OPI",brand5,45));
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Brand name you want to search :");
		String sbrand=sc.next();
		
		
		for(Product pro:products)
			if (pro.brand.getBrandname().equals(sbrand))
				System.out.println(pro);
	}
}