package com.week2.day2;

import java.util.ArrayDeque;

public class DequeImplementation {

	public static void main(String[] args) {
		ArrayDeque<Integer> d = new ArrayDeque<>();
		d.add(10);
		d.add(10);
		d.add(10);
		d.add(10);
		d.add(10);
		System.out.println(d);
		d.addFirst(30);
		System.out.println(d);
		System.out.println();
		d.addLast(40);
		System.out.println(d);
		System.out.println(d.poll());
		
		d.clone();
		System.out.println(d.isEmpty());

	}

}
